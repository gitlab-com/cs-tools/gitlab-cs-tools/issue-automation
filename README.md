# Issue automation

Automatically execute actions (like calling specific GitLab APIs) based on issues created.

## Features and limitations

* Using an issue template, issues with specific labels are created. On these issues, the description will have a known syntax
* For all open issues, based on label, process the issue using a function to handle it
  * Read the parameters necessary for the function by parsing the description markdown
  * Users must be instructed to not change the template for the automation to work
* Notify the user via notes on the issue on success or failure

## MVC: Allow Maintainers to set group variables

* The current iterations only supports one issue template [set_group_variables](.gitlab/templates/set_group_variables.md)
* Users can create issues using the template and specify their group, the group CI/CD variables they want to set and if they want to overwrite existing variables
* An issue is created with the label `group-variables`
* The issue is automatically set to `confidential` to prevent leakage of information
* When the pipeline runs, all open issues with label `group-variables` will be parsed and processed
  * The group path and author are called via the GitLab API
  * If the author has sufficient permissions on the group (higher than Maintainer) they request setting variables in, the automation will set the group variables specified in the issue description
    * if Overwrite is set to `True`, existing variables will be overwritten
* If an error occurs in the process, it is posted as a note on the issue and the label `error` is added
  * This allows users or support to correct the errors, delete the `error` label and the issue will be processed again in the next run
* If no error occurs, a success message is posted as a note on the issue and the label `success` is added
  * The issue will also be closed automatically

## Webhook triggering

You can trigger the pipeline via webhook on issue events. This is not yet implemented in this project though.

