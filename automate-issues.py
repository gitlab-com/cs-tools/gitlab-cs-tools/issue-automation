#!/usr/bin/env python3

import gitlab
import argparse
import markdown
import re
import xml.etree.ElementTree as ET
from distutils import util
from datetime import datetime

class Issue_Parser():

    gitlab_url = "https://gitlab.com/"
    gl = None
    project = None
    issues = []

    def __init__(self, args):
        if args.gitlab:
            self.gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
        self.gl = gitlab.Gitlab(self.gitlab_url, private_token = args.token)
        self.project = self.gl.projects.get(args.project)
        for issue in self.project.issues.list(as_list=False, state='opened'):
            self.issues.append(issue)

    def notify_via_comment(self, issue, comment, label):
        '''Post a note on the issue, update labels and close in case of success'''
        issue.notes.create({'body': comment})
        # we are using error label events to determine when the pipeline errored on the issue.
        # So if the label already has the error label, we have to remove it, then add it again, to get the proper time of error
        if label in issue.labels:
            issue.labels.remove(label)
            issue.save()
        issue.labels.append(label)
        issue.save()
        if label == "success":
            issue.state_event = 'close'
            issue.save()

    def get_group(self, issue, group_path):
        '''Get group specified in the issue from its path'''
        try:
            group = self.gl.groups.get(group_path, retry_transient_errors=True)
            return group
        except gitlab.exceptions.GitlabGetError as err:
            errormsg = "[ERROR] Target group not found **%s**" % group_path
            print(errormsg)
            self.notify_via_comment(issue, errormsg, "error")
            return None
        except gitlab.exceptions.GitlabHttpError as err:
            errormsg = "[ERROR] Can not get target group."
            print(errormsg)
            self.notify_via_comment(issue, errormsg, "error")
            print(err)
            return None

    def overwrite_group_variable(self, group, variable, issue, msg):
        has_error = False
        try:
            existing_variable = group.variables.get(variable["key"])
            existing_variable.value = variable["value"]
            existing_variable.variable_type = variable["variable_type"]
            existing_variable.protected = bool(util.strtobool(variable["protected"]))
            existing_variable.masked = bool(util.strtobool(variable["masked"]))
            existing_variable.save()
            msg += "Successfully updated variable **%s** in group **%s** to ```%s```\n\n" % (variable["key"], group.full_path, variable["value"])
        except gitlab.exceptions.GitlabGetError:
            try:
                var = group.variables.create(variable)
                msg += "Successfully configured variable **%s** in group **%s** to ```%s```\n\n" % (variable["key"], group.full_path, variable["value"])
            except gitlab.exceptions.GitlabHttpError as err:
                print("[ERROR] Could not create variable **%s** in group **%s** defined in issue **%s**" % (variable["key"], group.full_path, issue.web_url))
                print(err)
                has_error = True
                msg += "[ERROR] Could not create variable **%s** in group **%s**:\n" % (variable["key"], group.full_path)
                msg += "`%s`\n\n" % err
        except gitlab.exceptions.GitlabHttpError as err:
            print("[ERROR] Could not create variable **%s** in group **%s** defined in issue **%s**" % (variable["key"], group.full_path, issue.web_url))
            print(err)
            has_error = True
            msg += "[ERROR] Could not create variable **%s** in group **%s**:\n" % (variable["key"], group.full_path)
            msg += "`%s`\n\n" % err
        return msg, has_error

    def update_group_variables(self, group, variables, issue, overwrite):
        '''Update variables for a group, overwriting if key exists and overwrite == True'''
        msg = ""
        has_error = False
        for variable in variables:
            if overwrite:
                msg, has_error = self.overwrite_group_variable(group, variable, issue, msg)
            else:
                try:
                    var = group.variables.create(variable)
                    msg += "Successfully configured variable **%s** in group **%s** to ```%s```\n\n" % (variable["key"], group.full_path, variable["value"])
                except gitlab.exceptions.GitlabCreateError as err:
                    print("[ERROR] Could not create variable **%s** in group **%s** defined in issue **%s**" % (variable["key"], group.full_path, issue.web_url))
                    print(err)
                    has_error = True
                    msg += "[ERROR] Could not create variable **%s** in group **%s**:\n" % (variable["key"], group.full_path)
                    msg += "`%s`\n\n" % err
                except gitlab.exceptions.GitlabHttpError as err:
                    print("[ERROR] Could not create variable **%s** in group **%s** defined in issue **%s**" % (variable["key"], group.full_path, issue.web_url))
                    print(err)
                    has_error = True
                    msg += "[ERROR] Could not create variable **%s** in group **%s**:\n" % (variable["key"], group.full_path)
                    msg += "`%s`\n\n" % err
        if has_error:
            self.notify_via_comment(issue, msg, "error")
        else:
            self.notify_via_comment(issue, msg, "success")

    def verify_author_permissions(self, author_user, target_group):
        '''Verify the user opening the issue is member of the target group with access_level > 40 (maintainer or higher)'''
        try:
            member = target_group.members.get(author_user.id)
            #at least maintainer
            if member.access_level >= 40:
                return True
            else:
                return False
        except gitlab.exceptions.GitlabGetError as err:
            msg = "[ERROR] User **%s** not found in target group **%s**" % (author_user.username, target_group.full_path)
            print(msg)
            self.notify_via_comment(issue, msg, "error")
            return False

    def validate_masking_requirements(self, key, value):
        is_valid = True
        predefined_vars = ["CHAT_CHANNEL","CHAT_INPUT","CI","CI_API_V4_URL","CI_BUILDS_DIR","CI_COMMIT_BEFORE_SHA",
        "CI_COMMIT_DESCRIPTION","CI_COMMIT_MESSAGE","CI_COMMIT_REF_NAME","CI_COMMIT_REF_PROTECTED","CI_COMMIT_REF_SLUG",
        "CI_COMMIT_SHA","CI_COMMIT_SHORT_SHA","CI_COMMIT_BRANCH","CI_COMMIT_TAG","CI_COMMIT_TITLE","CI_COMMIT_TIMESTAMP",
        "CI_CONCURRENT_ID","CI_CONCURRENT_PROJECT_ID","CI_CONFIG_PATH","CI_DEBUG_TRACE","CI_DEFAULT_BRANCH",
        "CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX","CI_DEPENDENCY_PROXY_SERVER","CI_DEPENDENCY_PROXY_PASSWORD",
        "CI_DEPENDENCY_PROXY_USER","CI_DEPLOY_FREEZE","CI_DEPLOY_PASSWORD","CI_DEPLOY_USER","CI_DISPOSABLE_ENVIRONMENT",
        "CI_ENVIRONMENT_NAME","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_URL","CI_EXTERNAL_PULL_REQUEST_IID",
        "CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY","CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY",
        "CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME","CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA",
        "CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME","CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA",
        "CI_HAS_OPEN_REQUIREMENTS","CI_OPEN_MERGE_REQUESTS","CI_JOB_ID","CI_JOB_IMAGE","CI_JOB_MANUAL",
        "CI_JOB_NAME","CI_JOB_STAGE","CI_JOB_STATUS","CI_JOB_TOKEN","CI_JOB_JWT","CI_JOB_URL","CI_KUBERNETES_ACTIVE",
        "CI_MERGE_REQUEST_ASSIGNEES","CI_MERGE_REQUEST_ID","CI_MERGE_REQUEST_IID","CI_MERGE_REQUEST_LABELS",
        "CI_MERGE_REQUEST_MILESTONE","CI_MERGE_REQUEST_PROJECT_ID","CI_MERGE_REQUEST_PROJECT_PATH","CI_MERGE_REQUEST_PROJECT_URL",
        "CI_MERGE_REQUEST_REF_PATH","CI_MERGE_REQUEST_SOURCE_BRANCH_NAME","CI_MERGE_REQUEST_SOURCE_BRANCH_SHA",
        "CI_MERGE_REQUEST_SOURCE_PROJECT_ID","CI_MERGE_REQUEST_SOURCE_PROJECT_PATH","CI_MERGE_REQUEST_SOURCE_PROJECT_URL",
        "CI_MERGE_REQUEST_TARGET_BRANCH_NAME","CI_MERGE_REQUEST_TARGET_BRANCH_SHA","CI_MERGE_REQUEST_TITLE",
        "CI_MERGE_REQUEST_EVENT_TYPE","CI_MERGE_REQUEST_DIFF_ID","CI_MERGE_REQUEST_DIFF_BASE_SHA","CI_NODE_INDEX",
        "CI_NODE_TOTAL","CI_PAGES_DOMAIN","CI_PAGES_URL","CI_PIPELINE_ID","CI_PIPELINE_IID","CI_PIPELINE_SOURCE",
        "CI_PIPELINE_TRIGGERED","CI_PIPELINE_URL","CI_PROJECT_CONFIG_PATH","CI_PROJECT_DIR","CI_PROJECT_ID","CI_PROJECT_NAME",
        "CI_PROJECT_NAMESPACE","CI_PROJECT_ROOT_NAMESPACE","CI_PROJECT_PATH","CI_PROJECT_PATH_SLUG",
        "CI_PROJECT_REPOSITORY_LANGUAGES","CI_PROJECT_TITLE","CI_PROJECT_URL","CI_PROJECT_VISIBILITY","CI_REGISTRY",
        "CI_REGISTRY_IMAGE","CI_REGISTRY_PASSWORD","CI_REGISTRY_USER","CI_REPOSITORY_URL","CI_RUNNER_DESCRIPTION",
        "CI_RUNNER_EXECUTABLE_ARCH","CI_RUNNER_ID","CI_RUNNER_REVISION","CI_RUNNER_SHORT_TOKEN","CI_RUNNER_TAGS",
        "CI_RUNNER_VERSION","CI_SERVER","CI_SERVER_URL","CI_SERVER_HOST","CI_SERVER_PORT","CI_SERVER_PROTOCOL","CI_SERVER_NAME",
        "CI_SERVER_REVISION","CI_SERVER_VERSION","CI_SERVER_VERSION_MAJOR","CI_SERVER_VERSION_MINOR","CI_SERVER_VERSION_PATCH",
        "CI_SHARED_ENVIRONMENT","GITLAB_CI","GITLAB_FEATURES","GITLAB_USER_EMAIL","GITLAB_USER_ID","GITLAB_USER_LOGIN",
        "GITLAB_USER_NAME"]
        if "\n" in value:
            is_valid = False
        if not re.match(r"^[A-Za-z0-9+/=:@]{8,}$", value):
            is_valid = False
        if key in predefined_vars:
            is_valid = False
        return is_valid

    def validate_group_variable(self, variable, issue, variable_name = ""):
        fields = ["key","variable_type","value","protected","masked"]
        msg = ""
        error = False
        # add variable key in error message
        for field in fields:
            if field not in variable:
                msg += " [ERROR] Missing field **%s** in variable %s\n\n" % (field, variable_name)
                error = True
        if "variable_type" in variable:
            if variable["variable_type"] != "env_var" and variable["variable_type"] != "file":
                msg += "[ERROR] Invalid variable_type **%s** in %s. Should be \"env_var\" or \"file\".\n\n" % (variable["variable_type"], variable_name)
                error = True
        if "masked" in variable:
            if variable["masked"] not in ["true","false"]:
                msg += "[ERROR] Invalid value for **masked**: %s in %s. Should be \"true\" or \"false\".\n\n" % (variable["masked"], variable_name)
                error = True
            else:
                if variable["masked"] == "true" and "value" in variable:
                    if not self.validate_masking_requirements(variable["key"], variable["value"]):
                        msg += "[ERROR] %s does not adhere to [masking requirements](https://docs.gitlab.com/ee/ci/variables/README.html#masked-variable-requirements).\n\n" % (variable_name)
                        error = True
        if "protected" in variable:
            if variable["protected"] not in ["true","false"]:
                msg += "[ERROR] Invalid value for **protected**: %s in %s. Should be \"true\" or \"false\".\n\n" % (variable["protected"], variable_name)
                error = True
        if error:
            print(msg.strip())
            self.notify_via_comment(issue, msg, "error")
        return not error

    def set_group_variables(self, issue, root):
        '''Handle a "group-variables" issue:
        * get the group string and group object
        * get its author and verify their access_level
        * get configuration: Overwrite and variable configuration
        * trigger update
        '''
        author = issue.author["id"]
        author_user = self.gl.users.get(author)
        code_blocks = root.findall("./p/code")
        target_group = None
        overwrite = False

        if len(code_blocks) == 2:
            # not checking the name of the code blocks, just their position so this is a bit brittle to users diverging from template
            print("[Info] Target group is %s in %s" % (code_blocks[0].text, issue.web_url))
            target_group = self.get_group(issue, code_blocks[0].text)
            if not target_group:
                return None
            if len(code_blocks)>1:
                overwrite = bool(util.strtobool(code_blocks[1].text))
            print("[Info] Overwrite set to %s in %s" % (str(overwrite), issue.web_url))
        else:
            errormsg = "[ERROR] Target group not found, issue malformatted: %s" % issue.web_url
            print(errormsg)
            self.notify_via_comment(issue, errormsg, "error")
            return None

        if not self.verify_author_permissions(author_user, target_group):
            errormsg = "[ERROR] User **%s** has insufficient rights to change variables in group **%s**" % (author_user.username, target_group.full_path)
            print(errormsg)
            self.notify_via_comment(issue, errormsg, "error")
            return None

        variables = []
        variable_elements = root.findall("./ul")
        allchildren = list(root)
        variableheading = ""
        incorrect_variable = False
        for child in allchildren:
            if child.tag == "h2" and child.text.startswith("Variable"):
                variableheading = child.text
            elif child.tag == "ul":
                variable = {}
                for field in child:
                    # value needs to be in codeblock
                    if field.text.strip("\"").startswith("value"):
                        if list(field):
                            value = list(field)[0]
                            if value.tag == "code":
                                variable["value"] = value.text
                            else:
                                errormsg = "[ERROR] Variable value needs to be in a code block for **%s**" % (variableheading)
                                print(errormsg)
                                self.notify_via_comment(issue, errormsg, "error")
                                incorrect_variable = True
                        else:
                            errormsg = "[ERROR] Variable value needs to be in a code block for **%s**" % (variableheading)
                            print(errormsg)
                            self.notify_via_comment(issue, errormsg, "error")
                            incorrect_variable = True
                    # other fields can be split and used directly
                    else:
                        field_parts = field.text.split(":")
                        variable[field_parts[0].strip().strip("\"")] = field_parts[1].strip().strip("\"")
                var_is_valid = incorrect_variable if incorrect_variable else self.validate_group_variable(variable, issue, variableheading)
                if not var_is_valid:
                    incorrect_variable = True
                variables.append(variable)
        if not incorrect_variable:
            self.update_group_variables(target_group, variables, issue, overwrite)

    def was_updated_after_error(self, issue):
        #these are chronologically sorted, so newest ones will be at the bottom -> reverse to have them at the top
        label_events = issue.resourcelabelevents.list()
        label_events.reverse()
        error_date = None
        for event in label_events:
            if event.label["name"] == "error":
                error_date = datetime.strptime(event.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
                break
        updated_at = datetime.strptime(issue.updated_at, '%Y-%m-%dT%H:%M:%S.%fZ')
        if updated_at > error_date:
            return True
        return False

    def parse_issue(self, issue):
        '''
        Parse an issue objects description to an element tree for further processing
        Based on label, call a function to handle it
        '''
        html = "<root>" + markdown.markdown(issue.description) + "</root>"
        # parsing the html as xml to use paths
        try:
            root = ET.fromstring(html)
        except Exception:
            errormsg = "[ERROR] Could not parse issue: %s. Make sure to put XML values into a code block using \\`\\`\\`." % issue.web_url
            print(errormsg)
            self.notify_via_comment(issue, errormsg, "error")
            return

        # re-process errors if the issue was modified afterwards
        if "error" in issue.labels:
            if not self.was_updated_after_error(issue):
                return
        #use label to verify which action to take
        if "group-variables" in issue.labels:
            self.set_group_variables(issue, root)
        #else comment to use template

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create report for GitLab issues')
    parser.add_argument('token', help='API token able to make config changes on groups and projects. Could either be admin or in some cases group owner may be enough')
    parser.add_argument('project', help='Project ID to crawl for issues')
    parser.add_argument('--gitlab', help='GitLab URL, defaults to https://gitlab.com/')

    args = parser.parse_args()
    issue_parser = Issue_Parser(args)
    for issue in issue_parser.issues:
        print("[INFO] Processing issue %s" % issue.web_url)
        issue_parser.parse_issue(issue)
