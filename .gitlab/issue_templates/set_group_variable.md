# Set group variables

<!--Full path of your group including root namespace, i.e. for https://gitlab.com/gitlab-org/gitlab, the group should be `gitlab-org/gitlab`-->

Group: `gitlab-org/gitlab`

<!--Overwrite existing variables. If key already exists, overwrite it with values in this issue. `True` or `False`-->

Overwrite: `False`

<!--Variable definitions. Best copy and paste the example to avoid syntax errors.-->
<!--All fields must be defined for each variable-->
<!--variable_type can be a "env_var" (environment variable) or "file" (file type variable)-->
<!--variable value must be in code block: ```value```-->
<!--protected and masked must be either "true" or "false"-->
<!--Make sure masked variables adhere to masking requirements: https://docs.gitlab.com/ee/ci/variables/README.html#masked-variable-requirements-->

# Variables

## Variable 1

* "key": "variable_key"
* "variable_type": "env_var"
* "value": ```TEST_1```
* "protected": "false"
* "masked": "false"

## Variable 2

* "key": "variable_key"
* "variable_type": "env_var"
* "value": ```TEST_1```
* "protected": "false"
* "masked": "false"

<!--Don't change anything below this comment-->
/confidential
/label ~"group-variables"